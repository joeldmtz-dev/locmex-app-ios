//
//  Unit.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 30/05/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//
import Foundation
import UIKit

class Unit {
    var nid: String!
    var name: String!
    var address : String!
    var date : String!
    var speed : String!
    var direction : String!
    var imageIconUrl : String?
    var imageIcon : UIImage
    var lat : Double!
    var lng : Double!
    var status : String!
    
    init () {
        self.name = ""
        self.address = ""
        self.date = ""
        self.speed = ""
        self.direction = ""
        self.imageIcon = UIImage()
        self.imageIconUrl = ""
        self.lat = 0
        self.lng = 0
    }
    
    static func parse(from: NSDictionary!) -> Unit {
        let unit = Unit()
        
        let nid = from.object(forKey: "NID")
        let name = from.object(forKey: "NOM")
        let address = from.object(forKey: "DIR")
        let date = from.object(forKey: "TIM")
        let speed = from.object(forKey: "SPE")
        let direction = from.object(forKey: "CUR")
        let imageIconUrl = from.object(forKey: "ICO")
        let lat = from.object(forKey: "y")
        let lng = from.object(forKey: "x")
        let status = from.object(forKey: "STA")
        
        if Mirror(reflecting: nid!).subjectType == String.self {
            unit.nid = nid as! String
        } else {
            unit.nid = String(describing: nid!)
        }
        
        unit.name = name as! String
        unit.address = address as? String ?? "No disponible"
        unit.date = date as? String ?? ""
        
        if Mirror(reflecting: speed!).subjectType == String.self {
            unit.speed = speed as! String
        } else {
            unit.speed = String(describing: speed!)
        }
        
        if Mirror(reflecting: direction!).subjectType == String.self {
            unit.direction = direction as! String
        } else {
            unit.direction = String(describing: direction!)
        }
        
        unit.imageIconUrl = from.object(forKey: "ICO") as? String
        
        if let lat = lat as? String {
            unit.lat = Double(lat)
        } else {
            unit.lat = lat as! Double
        }
        
        if let lng = lng as? String {
            unit.lng = Double(lng
            )
        } else {
            unit.lng = lng as! Double
        }
        
        if Mirror(reflecting: status!).subjectType == String.self {
            unit.status = status as! String
        } else {
            unit.status = String(describing: status!)
        }
        
        unit.imageIconUrl = (imageIconUrl as! String)
        if let imgUrl = unit.imageIconUrl {
            if let url = URL(string: imgUrl) {
                do {
                    let data = try Data(contentsOf: url)
                    if let icon =  UIImage(data: data) {
                        unit.imageIcon = icon
                    }
                } catch {
                    print(error)
                }
            }
        }
        
        return unit
    }
}
