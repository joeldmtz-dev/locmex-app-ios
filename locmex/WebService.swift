//
//  WebService.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 01/06/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import PromiseKit

class WebService {
    
    static let BASE_URL = "http://gpslocmex.com";
    
    public static func getSSID (username: String!, password: String!) -> Promise<String> {
        return Promise { fullfill, reject in
            let url = "\(BASE_URL)/loginmobile/\(username!)/\(password!)"
            
            Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: .get)
                .validate()
                .responseString(completionHandler: { (response) in
                    switch response.result {
                    case .success(let nid):
                        fullfill(nid.trimmingCharacters(in: .whitespacesAndNewlines))
                    case .failure(let error):
                        reject(error)
                    }
                    
                })
            
        }
    }
    
    public static func getUnits(forSSID: String, byTimestamp: Int = 0) -> Promise<[NSDictionary]>{
        return Promise { fullfill, reject in
            let url = "\(BASE_URL)/unitsmobile/\(forSSID)/\(byTimestamp)"
            
            Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let dict):
                        fullfill(dict as! [NSDictionary])
                    case .failure(let error):
                        reject(error)
                    }
                })
            
        }
    }
    
    public static func getUnitData(forUnit: String, withSSID: String) -> Promise<[NSDictionary]>{
        return Promise { fullfill, reject in
            let url = "\(BASE_URL)/unitsmobileinfo2/\(withSSID)/\(forUnit)"
            
            Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let dict):
                        var data : [String : Any] = [:]
                        
                        for item in dict as! [NSDictionary] {
                            for (key, value) in item as! [String : Any] {
                                data[key] = "\(value)".trimmingCharacters(in: .whitespacesAndNewlines)
                            }
                        }
                        
                        fullfill([data] as [NSDictionary])
                    case .failure(let error):
                        reject(error)
                    }
                })
            
        }
    }
    
    public static func getGlobalAlerts(for ssid: String) -> Promise<[NSDictionary]> {
        return Promise { fullfill, reject in
            let url = "\(BASE_URL)/alertsmobile/\(ssid)/0/0/0/0"
            
            Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let dict):
                        fullfill(dict as! [NSDictionary])
                    case .failure(let error):
                        reject(error)
                    }
                })
            
        }
    }
    
    public static func getUnitAlerts(for nid: String, with ssid: String) -> Promise<[NSDictionary]> {
        return Promise { fullfill, reject in
            let url = "\(BASE_URL)/alertsmobile/\(ssid)/\(nid)/0/0/0"
            
            Alamofire.request(url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let dict):
                        fullfill(dict as! [NSDictionary])
                    case .failure(let error):
                        reject(error)
                    }
                })
            
        }
    }
    
}
