//
//  Alert.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 19/06/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit

class Alert {

    var unitName : String!
    var date : String!
    var alertName : String!
    
    init() {
        self.unitName = ""
        self.date = ""
        self.alertName = ""
    }
    
    static func parse(from: NSDictionary) -> Alert {
        let alert = Alert()
        
        alert.unitName = from.object(forKey: "unidad_nid") as? String ?? ""
        alert.date = from.object(forKey: "readable_time") as? String ?? "No disponible"
        alert.alertName = from.object(forKey: "notif_title") as? String ?? "No disponible"
        
        return alert
    }
    
}
