//
//  UnitAlertTableViewCell.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 20/06/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit

class UnitAlertTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var alertName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(from: Alert) {
        self.date.text = from.date
        self.alertName.text = from.alertName
    }

}
