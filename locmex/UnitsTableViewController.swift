//
//  UnitsTableViewController.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 29/05/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit
import WebKit
import UserNotifications
import DZNEmptyDataSet

class UnitsTableViewController: UITableViewController {
    
    let sharedData = DataShareService.sharedInstance
    
    // MARK: Components
    @IBOutlet weak var notificationButton: UIBarButtonItem!
    var searchController: UISearchController!
    var refreshController: UIRefreshControl!
    var webView : WKWebView!
    
    // MARK: Dataset
    var units: [Unit] = []
    var searchResults: [Unit] = []
    
    var indexToShow : Int?

    // MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()

        // Logo ImageVIew
        let imageView = UIImageView(image: #imageLiteral(resourceName: "locmex_name"))
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        // Tableview searchbar
        self.searchController = UISearchController(searchResultsController: nil)
        self.tableView.tableHeaderView = self.searchController.searchBar
        
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Buscar unidades"
        self.searchController.searchBar.setValue("Cancelar", forKey: "cancelButtonText")
        self.searchController.searchBar.barTintColor = AppColors.blue
        self.searchController.searchBar.tintColor = UIColor.white
        
        // Refresh controller
        self.refreshController = UIRefreshControl()
        self.refreshController.backgroundColor = AppColors.blue
        self.refreshController.tintColor = UIColor.white
        self.refreshController.addTarget(self, action: #selector(UnitsTableViewController.refresh), for: UIControlEvents.valueChanged)
        
        self.tableView.addSubview(self.refreshController!)
        
        // Tableview
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.tableFooterView = UIView()
        
        // Webview
        let contentController = WKUserContentController();
        contentController.add(self, name: "printTicket")
        contentController.add(self, name: "refreshNotification")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        config.websiteDataStore = WKWebsiteDataStore.nonPersistent()
        
        self.webView = WKWebView(frame: CGRect.zero, configuration: config)
        
        if let username = UserDefaults.standard.string(forKey: "username"), let password = UserDefaults.standard.string(forKey: "password") {
            let url = "http://gpslocmex.com/mobilesocket/\(username)/\(password)"
            let req = NSURLRequest(url: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!, cachePolicy:NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
            //var url = NSURL(fileURLWithPath: Bundle.main.path(forResource: "index", ofType: "html")!)
            //var req = NSURLRequest(url: url as URL)
            self.webView!.load(req as URLRequest)
        }
        
        // Initialize notifications user defaults
        if !UserDefaults.standard.bool(forKey: "notify_initialized") {
            UserDefaults.standard.set(true, forKey: "notify_initialized")
            UserDefaults.standard.set(true, forKey: "notify")
        }
        
        // Get units
        if let units = sharedData.units {
            self.units = units
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.indexToShow = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Logout
    @IBAction func logout(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Cerrar sessión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: .alert)
        let quitAction = UIAlertAction(title: "Salir", style: .destructive) { (alert: UIAlertAction) in
            self.sharedData.units = nil
            self.sharedData.currentUnit = nil
            UserDefaults.standard.set(nil, forKey: "NID")
            self.performSegue(withIdentifier: "unwindToLogin", sender: self)
        }
        
        let keepAction = UIAlertAction(title: "Cancelar", style: .default) { (alert: UIAlertAction) in
            
        }
        
        alertController.addAction(keepAction)
        alertController.addAction(quitAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    // MARK: Refresh tableview
    func refresh() {
        let ssid = UserDefaults.standard.string(forKey: "NID")!
        self.tableView.allowsSelection = false
        WebService
            .getUnits(forSSID: ssid)
            .then { (result) -> Void in
                
                var newUnits : [Unit] = []
                for unit in result {
                    newUnits.append(Unit.parse(from: unit as NSDictionary))
                }
                
                self.sharedData.units = newUnits
                self.units = newUnits
                
                self.refreshController.endRefreshing()
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.tableView.allowsSelection = true
                }
                
            }.catch { (error) in
                let alert = self.createRetryAlert(title: "Algo salio mal", message: "No fue posible obtener la información") {
                    self.refresh()
                }
                self.present(alert, animated: true)
                self.tableView.allowsSelection = true
        }
    }
    
    func createRetryAlert(title: String, message: String, callback: @escaping () -> Void) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Reintentar", style: .default) { (alert: UIAlertAction) in
            callback()
        }
        
        let quitAction = UIAlertAction(title: "Cancelar", style: .destructive) { (alert: UIAlertAction) in
            self.refreshController.endRefreshing()
        }
        
        alertController.addAction(retryAction)
        alertController.addAction(quitAction)
        
        return alertController
        
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchController.isActive {
            return self.searchResults.count
        } else {
            return self.units.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UnitCell", for: indexPath) as! UnitTableViewCell

        let unit: Unit!
        
        if self.searchController.isActive {
            unit = self.searchResults[indexPath.row]
        } else {
            unit = self.units[indexPath.row]
        }
        
        cell.bind(from: unit)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if self.searchController.isActive {
            sharedData.currentUnit = self.searchResults[indexPath.row]
            self.searchController.isActive = false
        } else {
            sharedData.currentUnit = self.units[indexPath.row]
            self.searchController.isActive = false
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.refreshController.isRefreshing {
            return false
        }
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if self.searchController.isActive {
            sharedData.currentUnit = self.searchResults[indexPath.row]
            self.searchController.isActive = false
        } else {
            sharedData.currentUnit = self.units[indexPath.row]
            self.searchController.isActive = false
        }
        
        let gotoMap = UITableViewRowAction(style: .default, title: "\n\nMapa") { (action, indexPath) in
            self.indexToShow = 0
            self.performSegue(withIdentifier: "unitsToDetail", sender: self)
        }
        
        let imgMap: UIImage = UIImage(named: "map_large")!
        let imgMapSize: CGSize = tableView.frame.size
        UIGraphicsBeginImageContext(imgMapSize)
        var context = UIGraphicsGetCurrentContext()
        context!.setFillColor(AppColors.red.cgColor)
        context!.fill(CGRect(x: 0, y: 0, width: 80, height: self.tableView.rowHeight))
        imgMap.draw(in: CGRect(x: 17, y: 10, width: 40, height: 40))
        let newMapImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        gotoMap.backgroundColor = UIColor(patternImage: newMapImage)

        
        let gotoDetails = UITableViewRowAction(style: .destructive, title: "\n\nDetalles") { (action, indexPath) in
            self.indexToShow = 1
            self.performSegue(withIdentifier: "unitsToDetail", sender: self)
        }
        
        gotoDetails.backgroundColor = AppColors.yellow
        let imgDetails: UIImage = UIImage(named: "details_large")!
        let imgDetailsSize: CGSize = tableView.frame.size
        UIGraphicsBeginImageContext(imgDetailsSize)
        context = UIGraphicsGetCurrentContext()
        context!.setFillColor(AppColors.blue.cgColor)
        context!.fill(CGRect(x: 0, y: 0, width: 100, height: self.tableView.rowHeight))
        imgDetails.draw(in: CGRect(x: 25, y: 10, width: 40, height: 40))
        let newDetailsImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        gotoDetails.backgroundColor = UIColor(patternImage: newDetailsImage)
        
        let gotoAlerts = UITableViewRowAction(style: .default, title: "\n\nAlertas") { (action, indexPath) in
            self.indexToShow = 2
            self.performSegue(withIdentifier: "unitsToDetail", sender: self)
        }
        
        let imgAlert: UIImage = UIImage(named: "alert_large")!
        let imgAlertSize: CGSize = tableView.frame.size
        UIGraphicsBeginImageContext(imgAlertSize)
        context = UIGraphicsGetCurrentContext()
        context!.setFillColor(AppColors.yellow.cgColor)
        context!.fill(CGRect(x: 0, y: 0, width: 90, height: self.tableView.rowHeight))
        imgAlert.draw(in: CGRect(x: 25, y: 10, width: 40, height: 40))
        let newAlertImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        gotoAlerts.backgroundColor = UIColor(patternImage: newAlertImage)
        
        return [gotoAlerts, gotoDetails, gotoMap]
    }

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let backItem = UIBarButtonItem()
        backItem.title = "Unidades"
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == "unitsToDetail" && self.indexToShow != nil {
            if let tabs = segue.destination as? UITabBarController {
                tabs.selectedIndex = self.indexToShow!
                
                
            }
        } else if segue.identifier == "MainToGlobalAlerts" {
            UIApplication.shared.applicationIconBadgeNumber = 0
            UserDefaults.standard.set(0, forKey: "unread_notifications")
            self.navigationItem.rightBarButtonItem?.badgeValue = "0"
        }
    }
    
    // MARK: Filter method
    func filterContentFor(textToSearch: String){
        if textToSearch == "" {
            self.searchResults = self.units
        } else {
            self.searchResults = self.units.filter({ (unit) -> Bool in
                let nameToFind = unit.name.range(of: textToSearch, options: NSString.CompareOptions.caseInsensitive)
                let addressToFind = unit.address.range(of: textToSearch, options: NSString.CompareOptions.caseInsensitive)
                return nameToFind != nil || addressToFind != nil
            })
        }
    }
    
}

// MARK: Empty dataset delegate
extension UnitsTableViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "unit")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No tiene unidades disponibles", attributes: [:])
    }
    
}

// MARK: Searchresult updating delegate
extension UnitsTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = self.searchController.searchBar.text {
            self.filterContentFor(textToSearch: searchText)
            self.tableView.reloadData()
        }
    }
    
}

// MARK: Webview delegate
extension UnitsTableViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if(message.name == "printTicket") {
            if let data: NSData = (message.body as! String).data(
                using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) as NSData?{
                
                do{
                    if let jsonObj = try JSONSerialization.jsonObject(
                        with: data as Data,
                        options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [Dictionary<String, AnyObject>]{
                        
                        let newUnit = jsonObj[0]
                        var row : Int = 0
                        for unit in self.units {
                            if Int(unit.nid!) == (newUnit["NID"] as! Int) {
                                break
                            }
                            
                            row += 1
                        }
                        
                        if row < self.units.count {
                            let unit = Unit.parse(from: newUnit as NSDictionary)
                            
                            self.units[row].address = unit.address
                            self.units[row].date = unit.date
                            self.units[row].direction = unit.direction
                            self.units[row].lat = unit.lat
                            self.units[row].lng = unit.lng
                            self.units[row].speed = unit.speed
                            self.units[row].status = unit.status
                            
                            DispatchQueue.main.async {
                                self.tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .fade)
                            }
                        }
                    }
                } catch{
                    print("Error \(error)")
                }
            }
            
        } else if (message.name == "refreshNotification") {
            if let data: NSData = (message.body as! String).data(
                using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) as NSData?{
                
                do{
                    if let jsonObj = try JSONSerialization.jsonObject(
                        with: data as Data,
                        options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [Dictionary<String, AnyObject>]{
                        
                        let notificationText = jsonObj[0]
                        
                        let unread = UserDefaults.standard.integer(forKey: "unread_notifications") + 1
                        self.navigationItem.rightBarButtonItem?.badgeValue = "\(unread)"
                        
                        if UserDefaults.standard.bool(forKey: "notify") {
                            if #available(iOS 10.0, *) {
                                let content = UNMutableNotificationContent()
                                content.title = "Nueva alerta"
                                
                                if unread > 1 {
                                    content.body = "\(unread) notificaciones pendientes"
                                } else {
                                    content.body = notificationText["notif_title"] as! String
                                }
                                
                                content.sound = UNNotificationSound.default()
                                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                                let request = UNNotificationRequest(identifier: "alertNotification", content: content, trigger: trigger)
                                
                                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                                UNUserNotificationCenter.current().delegate = self
                                UNUserNotificationCenter.current().add(request) {(error) in
                                    if let error = error {
                                        print("Error: \(error)")
                                    }
                                }
                            } else {
                                let notification = UILocalNotification()
                                notification.fireDate = Date(timeIntervalSinceNow: 5)
                                
                                if unread > 1 {
                                    notification.alertBody = "\(unread) notificaciones pendientes"
                                } else {
                                    notification.alertBody = (notificationText["notif_title"] as! String)
                                }
                                
                                notification.timeZone = NSTimeZone.default
                                notification.soundName = UILocalNotificationDefaultSoundName
                                UIApplication.shared.cancelAllLocalNotifications()
                                UIApplication.shared.scheduleLocalNotification(notification)
                            }
                        }
                    }
                } catch{
                    print("Error: \(error)")
                }
            }
        }
    }
    
}

// MARK: User notification delegate
extension UnitsTableViewController: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
}
