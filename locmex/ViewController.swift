//
//  ViewController.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 27/05/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ViewController: UIViewController, UITextFieldDelegate {

    let sharedData = DataShareService.sharedInstance
    
    // MARK: Components
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loadingView: UIView!
    
    // MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.loginButton.layer.cornerRadius = 10
        
        let activityIndicatorView = NVActivityIndicatorView(
            frame: CGRect(x: 0, y: 0, width: 50, height: 50),
            type: .ballScaleRippleMultiple,
            color: UIColor.white
        )
        
        self.username.layer.borderColor = UIColor.red.cgColor
        self.username.layer.cornerRadius = 5.0
        self.password.layer.borderColor = UIColor.red.cgColor
        self.password	.layer.cornerRadius = 5.0
        
        self.username.delegate = self
        self.password.delegate = self
        
        self.loadingView.addSubview(activityIndicatorView)
        self.loadingView.backgroundColor = UIColor.clear
        activityIndicatorView.startAnimating()
        
        if let username = UserDefaults.standard.string(forKey: "username"), let password = UserDefaults.standard.string(forKey: "password") {
            self.loginRequest(username: username, password: password)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.string(forKey: "NID") != nil	 {
            self.showLoading()
        } else {
            self.hideLoading()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Textfield Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let field = textField.superview?.viewWithTag(textField.tag + 1)
        
        if let nextField = field as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.login(self.loginButton)
        }
        
        return false
    }
    
    // MARK: Loading
    func showLoading() {
        self.username.isHidden = true
        self.password.isHidden = true
        self.loginButton.isHidden = true
        self.loadingView.isHidden = false
    }
    
    func hideLoading() {
        self.loadingView.isHidden = true
        self.username.isHidden = false
        self.password.isHidden = false
        self.loginButton.isHidden = false
    }


    // MARK: Login
    @IBAction func login(_ sender: UIButton) {
        var valid = true
        self.username.layer.borderWidth = 0.0
        self.password.layer.borderWidth = 0.0
        
        if self.username.text == "" {
            valid = false
            self.username.layer.borderWidth = 2.0
        }
        
        if self.password.text == "" {
            valid = false
            self.password.layer.borderWidth = 2.0
        }
        
        if valid {
            loginRequest(username: username.text!, password: password.text!)
        }
    }
    
    public func loginRequest (username: String, password: String) {
        self.showLoading()
        
        WebService.getSSID(username: username, password: password)
            .then { (result) -> Void in
                
                if (result == "MOROSO") {
                    UserDefaults.standard.set(nil, forKey: "NID")
                    UserDefaults.standard.set(nil, forKey: "username")
                    UserDefaults.standard.set(nil, forKey: "password")
                    
                    let alertController = UIAlertController(title: "No es posible ingresar", message: "Usted cuenta con un adeudo", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Entendido", style: .default)
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    self.hideLoading()
                    self.password.text = ""
                    return
                }
                
                if (result.characters.count > 0) {
                    self.username.text = ""
                    self.password.text = ""
                    UserDefaults.standard.set(result, forKey: "NID")
                    UserDefaults.standard.set(username, forKey: "username")
                    UserDefaults.standard.set(password, forKey: "password")
                    
                    self.loadData(SSID: result)
                } else {
                    let alertController = UIAlertController(title: "Error en el login", message: "Los datos introducidos son incorrectos", preferredStyle: .alert)
                    
                    self.hideLoading()
                    self.password.text = ""
                    let quitAction = UIAlertAction(title: "Cerrar", style: .default)
                    alertController.addAction(quitAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }.catch { (error) in
                let alert = self.createRetryAlert(title: "Algo salio mal", message: "No fue posible obtener la información") {
                    self.loginRequest(username: username, password: password)
                }
                self.present(alert, animated: true)
        }
    }
    
    public func loadData(SSID: String) {
        var units : [Unit] = []
        
        WebService
            .getUnits(forSSID: SSID)
            .then { (result) -> Void in
                
                for unit in result {
                    units.append(Unit.parse(from: unit as NSDictionary))
                }
                
                self.sharedData.units = units
                self.performSegue(withIdentifier: "loginToMain", sender: self)
                
            }.catch { (error) in
                let alert = self.createRetryAlert(title: "Algo salio mal", message: "No fue posible obtener la información") {
                    self.loadData(SSID: SSID)
                }
                self.present(alert, animated: true)
            }
    }
    
    func createRetryAlert(title: String, message: String, callback: @escaping () -> Void) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Reintentar", style: .default) { (alert: UIAlertAction) in
            callback()
        }
        
        let quitAction = UIAlertAction(title: "Salir", style: .destructive) { (alert: UIAlertAction) in
            exit(0)
        }
        
        alertController.addAction(retryAction)
        alertController.addAction(quitAction)
        
        return alertController
        
    }
    
    // MARK: Unwind
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
        
    }
}



