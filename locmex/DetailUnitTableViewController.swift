//
//  DetailUnitTableViewController.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 01/06/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class DetailUnitTableViewController: UITableViewController {
    
    var unit : Unit = Unit()
    let sharedData = DataShareService.sharedInstance
    
    // MARK: Components
    @IBOutlet weak var imgWeather: UIImageView!
    @IBOutlet weak var labelWeater: UILabel!
    @IBOutlet weak var tempWeater: UILabel!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var speed: UILabel!
    @IBOutlet weak var adress: UILabel!
    @IBOutlet weak var voltBattery: UILabel!
    @IBOutlet weak var percentageBattery: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var imei: UILabel!
    @IBOutlet weak var streetviewImg: UIImageView!
    @IBOutlet weak var directionIcon: UIImageView!
    
    var activityIndicatorView: NVActivityIndicatorView!
    var overlay : UIView!

    // MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.unit = self.sharedData.currentUnit ?? Unit()
        self.tabBarController?.navigationItem.title = self.unit.name
        self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 48, 0);
        self.tableView.tableFooterView = UIView()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
        
        self.name.text = self.unit.name
        self.date.text = self.unit.date
        self.status.text = "Encendida"
        self.speed.text = self.unit.speed
        self.adress.text = self.unit.address
        
        self.activityIndicatorView = NVActivityIndicatorView(
            frame: CGRect(x: 0, y: 0, width: 50, height: 50),
            type: .ballScaleRippleMultiple,
            color: UIColor.gray
        )
        
        self.overlay = UIView(frame: CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.origin.y, width: self.tableView.frame.width, height: self.tableView.frame.height))
        
        self.tableView.addSubview(overlay);
        self.tableView.isScrollEnabled = false
        self.overlay.layer.zPosition = 100
        self.overlay.backgroundColor = UIColor.white
        
        self.activityIndicatorView.center = CGPoint(x: overlay.frame.size.width / 2, y: (overlay.frame.size.height / 2) - 50)
        self.activityIndicatorView.startAnimating()
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: overlay.frame.size.width, height: 21))
        label.center = CGPoint(x: overlay.frame.size.width / 2, y: (overlay.frame.size.height / 2) + 10)
        label.textAlignment = .center
        //label.textColor = UIColor(red: 29.0/255.0, green: 54.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        label.textColor = UIColor.gray
        label.text = "Cargando"
        self.overlay.addSubview(label)
        
        self.overlay.addSubview(self.activityIndicatorView)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*// MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/
    
    func loadData() {
        self.overlay.isHidden = false
        WebService.getUnitData(forUnit: self.unit.nid, withSSID: UserDefaults.standard.string(forKey: "NID")!)
            .then { (result) -> Void in
                
                self.tableView.isScrollEnabled = true
                self.overlay.isHidden = true
                self.status.text = result[0].object(forKey: "Estatus") as? String ?? "No disponible"
                self.speed.text = result[0].object(forKey: "Velocidad") as? String ?? "No disponible"
                self.voltBattery.text = result[0].object(forKey: "Voltaje bateria") as? String ?? "No disponible"
                self.percentageBattery.text = result[0].object(forKey: "% bateria") as? String ?? "No disponible"
                self.temp.text = result[0].object(forKey: "Temperatura") as? String ?? "No disponible"
                self.phone.text = result[0].object(forKey: "Telefono") as? String ?? "No disponible"
                self.imei.text = result[0].object(forKey: "Imei") as? String ?? "No disponible"
                
                if let course = result[0].object(forKey: "Course") as? String {
                    self.directionIcon.transform = CGAffineTransform(rotationAngle: CGFloat(Float(course)!) * CGFloat(Double.pi) / 180.0)
                }
                
                if let imgUrl = result[0].object(forKey: "urlimg") as? String {
                    if let url = URL(string: imgUrl) {
                        do {
                            let data = try Data(contentsOf: url)
                            if let img =  UIImage(data: data) {
                                self.streetviewImg.image = img
                            }
                        } catch {
                            print(error)
                        }
                    }
                }
                
                if let weatherImg = result[0].object(forKey: "urliconoclima") as? String {
                    if let urlWeather = URL(string: weatherImg) {
                        do {
                            let data = try Data(contentsOf: urlWeather)
                            if let img =  UIImage(data: data) {
                                self.imgWeather.image = img
                            }
                        } catch {
                            print(error)
                        }
                    }
                }
                
                self.labelWeater.text = (result[0].object(forKey: "clima") as? String ?? "No disponible").capitalized
                self.tempWeater.text = "\(result[0].object(forKey: "temp") as? String ?? "NA")°"
                
            }.catch { (error) in
                let alert = self.createRetryAlert(title: "Algo salio mal", message: "No fue posible obtener la información") {
                    self.loadData()
                }
                self.present(alert, animated: true)
        }
    }
    
    func createRetryAlert(title: String, message: String, callback: @escaping () -> Void) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Reintentar", style: .default) { (alert: UIAlertAction) in
            callback()
        }
        
        let quitAction = UIAlertAction(title: "Regresar", style: .destructive) { (alert: UIAlertAction) in
            self.tabBarController?.selectedIndex = 0
        }
        
        alertController.addAction(retryAction)
        alertController.addAction(quitAction)
        
        return alertController
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        
        if indexPath.section == 1 && indexPath.row == 10 {
            return 100
        }
        
        return 60
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
