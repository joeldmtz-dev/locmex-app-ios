//
//  MapViewController.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 29/05/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit
import GoogleMaps


class MapViewController: UIViewController {
    
    var unit : Unit = Unit()
    let sharedData = DataShareService.sharedInstance
    var mapView : GMSMapView!

    // MARK: Components
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var speed: UILabel!
    @IBOutlet weak var direction: UIImageView!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    
    // MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.navigationItem.title = self.unit.name
        
        self.address.text = self.unit.address
        self.speed.text = self.unit.speed
        self.speed.layer.cornerRadius = 8
        self.speed.layer.masksToBounds = true
        
        if Int(self.unit.speed) == 0 && self.unit.status == "0" {
            self.speed.backgroundColor = AppColors.unit_off
        } else if Int(self.unit.speed)! < 20 && self.unit.status == "0" {
            self.speed.backgroundColor = AppColors.unit_off
        } else if Int(self.unit.speed)! == 0 && self.unit.status == "1" {
            self.speed.backgroundColor = AppColors.unit_stoped
        } else if Int(self.unit.speed)! > 0 && self.unit.status == "1" {
            self.speed.backgroundColor = AppColors.unit_driving
        }
        
        self.date.text = self.unit.date
        self.direction.transform = CGAffineTransform(rotationAngle: CGFloat(Float(self.unit.direction)!) * CGFloat(Double.pi) / 180.0)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        self.unit = self.sharedData.currentUnit ?? Unit()
        let camera = GMSCameraPosition.camera(withLatitude: unit.lat, longitude: unit.lng, zoom: 16.0)
        self.mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        self.mapView.isMyLocationEnabled = true
        self.mapView.mapType = GMSMapViewType.satellite
        self.view.insertSubview(self.mapView, at: 0)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: unit.lat, longitude: unit.lng)
        marker.title = unit.name
        marker.snippet = unit.address
        marker.icon = unit.imageIcon
        marker.map = self.mapView
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Map controls
    @IBAction func centerUnit(_ sender: UIButton) {
        let camera = GMSCameraPosition.camera(withLatitude: unit.lat, longitude: unit.lng, zoom: 16.0)
        self.mapView.animate(to: camera)
    }
    
    
    @IBAction func centerCurrentLocation(_ sender: UIButton) {
        let currentLocation = self.mapView.myLocation?.coordinate
        
        if let latitude = currentLocation?.latitude, let longitude = currentLocation?.longitude {
            let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 16.0)
            self.mapView.animate(to: camera)
        }
    }
    

}
