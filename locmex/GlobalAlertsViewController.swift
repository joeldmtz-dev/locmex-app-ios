//
//  GlobalAlertsViewController.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 19/06/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import NVActivityIndicatorView

class GlobalAlertsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    var alerts : [Alert] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    var activityIndicatorView: NVActivityIndicatorView!
    var overlay : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        let notify : Bool! = UserDefaults.standard.bool(forKey: "notify")
        self.notificationSwitch.setOn(notify, animated: true)
        
        self.activityIndicatorView = NVActivityIndicatorView(
            frame: CGRect(x: 0, y: 0, width: 50, height: 50),
            type: .ballScaleRippleMultiple,
            color: UIColor.gray
        )
        
        self.overlay = UIView(frame: self.tableView.bounds)
        
        self.tableView.addSubview(overlay);
        self.tableView.isScrollEnabled = false
        self.overlay.layer.zPosition = 100
        self.overlay.backgroundColor = UIColor.white
        
        self.activityIndicatorView.center = CGPoint(x: self.view.frame.size.width / 2, y: (self.view.frame.size.height / 2) - 50)
        self.activityIndicatorView.startAnimating()
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 21))
        label.center = CGPoint(x: self.view.frame.size.width / 2, y: (self.view.frame.size.height / 2) + 10)
        label.textAlignment = .center
        label.textColor = UIColor.gray
        label.text = "Cargando"
        self.overlay.addSubview(label)
        
        self.overlay.addSubview(self.activityIndicatorView)

        self.loadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.alerts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GlobalAlertCell", for: indexPath) as! AlertTableViewCell
        
        let alert = self.alerts[indexPath.row]
        
        cell.bind(from: alert)
        
        return cell
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "warning")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No tiene alertas", attributes: [:])
    }
    
    func loadData() {
        self.overlay.isHidden = false
        
        WebService.getGlobalAlerts(for: UserDefaults.standard.string(forKey: "NID")!)
            .then { (response) -> Void in
                
                var alerts : [Alert] = []
                for alert in response {
                    alerts.append(Alert.parse(from: alert))
                }
                
                self.alerts = alerts
                
                DispatchQueue.main.async {
                    self.overlay.isHidden = true
                    self.tableView.reloadData()
                }
            }.catch { (error) in
                let alert = self.createRetryAlert(title: "Algo salio mal", message: "No fue posible obtener la información") {
                    self.loadData()
                }
                
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    func createRetryAlert(title: String, message: String, callback: @escaping () -> Void) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Reintentar", style: .default) { (alert: UIAlertAction) in
            callback()
        }
        
        let quitAction = UIAlertAction(title: "Cerrar", style: .destructive) { (alert: UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(retryAction)
        alertController.addAction(quitAction)
        
        return alertController
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onNotificationSwitchChanged(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: "notify")
    }
    
}
