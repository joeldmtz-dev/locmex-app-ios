//
//  DataShareService.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 30/05/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

class DataShareService {
    
    static let sharedInstance = DataShareService()
    
    var currentUnit : Unit?
    var units : [Unit]?
    
    private init() {
        
    }
}
