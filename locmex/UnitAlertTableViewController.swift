//
//  UnitAlertTableViewController.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 20/06/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import DZNEmptyDataSet

class UnitAlertTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    var alerts : [Alert] = []
    var activityIndicatorView: NVActivityIndicatorView!
    var overlay : UIView!
    
    var unit : Unit = Unit()
    let sharedData = DataShareService.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        self.unit = self.sharedData.currentUnit ?? Unit()
        self.tabBarController?.navigationItem.title = self.unit.name
        
        self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 48, 0);
        self.tableView.tableFooterView = UIView()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        
        self.activityIndicatorView = NVActivityIndicatorView(
            frame: CGRect(x: 0, y: 0, width: 50, height: 50),
            type: .ballScaleRippleMultiple,
            color: UIColor.gray
        )
        
        self.overlay = UIView(frame: CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.origin.y, width: self.tableView.frame.width, height: self.tableView.frame.height))
        
        self.tableView.addSubview(overlay);
        self.tableView.isScrollEnabled = false
        self.overlay.layer.zPosition = 100
        self.overlay.backgroundColor = UIColor.white
        
        self.activityIndicatorView.center = CGPoint(x: overlay.frame.size.width / 2, y: (overlay.frame.size.height / 2) - 50)
        self.activityIndicatorView.startAnimating()
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: overlay.frame.size.width, height: 21))
        label.center = CGPoint(x: overlay.frame.size.width / 2, y: (overlay.frame.size.height / 2) + 10)
        label.textAlignment = .center
        label.textColor = UIColor.gray
        label.text = "Cargando"
        self.overlay.addSubview(label)
        
        self.overlay.addSubview(self.activityIndicatorView)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.alerts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UnitAlertCell", for: indexPath) as! UnitAlertTableViewCell
        
        cell.bind(from: self.alerts[indexPath.row])

        return cell
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "warning")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No tiene alertas", attributes: [:])
    }
    
    func loadData() {
        self.overlay.isHidden = false
        WebService.getUnitAlerts(for: self.unit.nid, with: UserDefaults.standard.string(forKey: "NID")!)
            .then { (response) -> Void in
                
                var alerts : [Alert] = []
                for alert in response {
                    alerts.append(Alert.parse(from: alert))
                }
                
                self.alerts = alerts
                
                DispatchQueue.main.async {
                    self.overlay.isHidden = true
                    self.tableView.reloadData()
                }
            }.catch { (error) in
                let alert = self.createRetryAlert(title: "Algo salio mal", message: "No fue posible obtener la información") {
                    self.loadData()
                }
                
                self.present(alert, animated: true, completion: nil)
        }
    }
    
    func createRetryAlert(title: String, message: String, callback: @escaping () -> Void) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Reintentar", style: .default) { (alert: UIAlertAction) in
            callback()
        }
        
        let quitAction = UIAlertAction(title: "Regresar", style: .destructive) { (alert: UIAlertAction) in
            self.tabBarController?.selectedIndex = 0
        }
        
        alertController.addAction(retryAction)
        alertController.addAction(quitAction)
        
        return alertController
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
