//
//  UnitTableViewCell.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 29/05/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit

class UnitTableViewCell: UITableViewCell {

    @IBOutlet weak var unitImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var speed: UILabel!
    @IBOutlet weak var curIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(from: Unit!) {
        self.name.text = from.name
        self.address.text = from.address
        self.date.text = from.date
        self.speed.text = from.speed
        self.speed.layer.cornerRadius = 8
        self.speed.layer.masksToBounds = true
        self.unitImage.image = from.imageIcon
        
        if let direction = Float(from.direction) {
            self.curIcon.transform = CGAffineTransform(rotationAngle: CGFloat(direction) * CGFloat(Double.pi) / 180.0)
        }
        
        if Int(from.speed) == 0 && from.status == "0" {
            self.speed.backgroundColor = AppColors.unit_off
        } else if Int(from.speed)! < 20 && from.status == "0" {
            self.speed.backgroundColor = AppColors.unit_off
        } else if Int(from.speed)! == 0 && from.status == "1" {
            self.speed.backgroundColor = AppColors.unit_stoped
        } else if Int(from.speed)! > 0 && from.status == "1" {
            self.speed.backgroundColor = AppColors.unit_driving
        }
        
    }

}
