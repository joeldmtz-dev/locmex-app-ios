//
//  Colors.swift
//  locmex
//
//  Created by Localizadores Mexicanos sa de cv on 07/06/17.
//  Copyright © 2017 Localizadores Mexicanos sa de cv. All rights reserved.
//

import UIKit

class AppColors {
    static var blue = UIColor(red: 29.0/255.0, green: 54.0/255.0, blue: 91.0/255.0, alpha: 1.0)
    static var red = UIColor(red: 206.0/255.0, green: 41.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    static var yellow = UIColor(red: 255.0/255.0, green: 201.00/255.0, blue: 56.0/255.0, alpha: 1.0)
    
    static var unit_driving = UIColor(red: 76.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    static var unit_stoped = UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static var unit_off = UIColor(red: 146.0/255.0, green: 146.0/255.0, blue: 146.0/255.0, alpha: 1.0)
}
